package kilya.iut.r5a05.role;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/role")
public class RoleController {
    private final RoleRepository roles;

    RoleController(RoleRepository roles) {
        this.roles = roles;
    }

    @GetMapping("")
    List<Role> all() {
        return StreamSupport.stream(roles.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    Role byId(@PathVariable Long id) {
        return roles.findById(id).orElseThrow();
    }
}
