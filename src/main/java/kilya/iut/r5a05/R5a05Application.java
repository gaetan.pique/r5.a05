package kilya.iut.r5a05;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class R5a05Application {
    public static void main(String[] args) {
        SpringApplication.run(R5a05Application.class, args);
    }

    @GetMapping("/bonjour")
    public String hello() {
        return "Bonjour le monde !";
    }
}
