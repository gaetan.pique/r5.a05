package kilya.iut.r5a05.securite;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class JwtDTO {
    private final String token;
    private final String type = "Bearer";
    private final Long id;
    private final String username;
    private final List<String> roles;
}
