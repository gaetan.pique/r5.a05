package kilya.iut.r5a05.securite;

import kilya.iut.r5a05.role.RoleRepository;
import kilya.iut.r5a05.utilisateur.Utilisateur;
import kilya.iut.r5a05.utilisateur.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/login")
public class LoginController {

    AuthenticationManager authenticationManager;
    TokenGenerator tokenGenerator;

    public LoginController(
            @Autowired AuthenticationManager _authenticationManager,
            @Autowired TokenGenerator _tokenGenerator
    ) {
        this.authenticationManager = _authenticationManager;
        this.tokenGenerator = _tokenGenerator;
    }

    @PostMapping("")
    JwtDTO login(@RequestBody LoginCommand _login) {
        Authentication authenticated = this.authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(_login.getUsername(), _login.getPassword()));

        String jwt = tokenGenerator.generateJwtToken(authenticated);

        Utilisateur utilisateur = (Utilisateur) authenticated.getPrincipal();
        List<String> roles = utilisateur.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.toList());

        return new JwtDTO(jwt,
                utilisateur.getUtilisateurId(),
                utilisateur.getUsername(),
                roles);
    }
}
