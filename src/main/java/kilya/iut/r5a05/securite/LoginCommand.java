package kilya.iut.r5a05.securite;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginCommand {
    private final String username;
    private final String password;
}
