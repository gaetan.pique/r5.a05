package kilya.iut.r5a05.utilisateur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceR5A05 implements UserDetailsService {
    private final UtilisateurRepository utilisateurs;

    public UserDetailsServiceR5A05(@Autowired UtilisateurRepository utilisateurs) {
        this.utilisateurs = utilisateurs;
    }

    @Override
    public UserDetails loadUserByUsername(String _username) throws UsernameNotFoundException {
        return utilisateurs.findByLogin(_username);
    }
}
