package kilya.iut.r5a05.utilisateur;

import org.springframework.data.repository.CrudRepository;

public interface UtilisateurRepository extends CrudRepository<Utilisateur, Long> {
    public Utilisateur findByLogin(String _login);
}
