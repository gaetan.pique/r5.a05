package kilya.iut.r5a05.utilisateur;

import kilya.iut.r5a05.role.RoleRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/utilisateur")
public class UtilisateurController {
    private final UtilisateurRepository utilisateurs;
    private final RoleRepository roles;

    UtilisateurController(
            UtilisateurRepository utilisateurs,
            RoleRepository roles
    ) {
        this.utilisateurs = utilisateurs;
        this.roles = roles;
    }

    @GetMapping("")
    List<Utilisateur> all() {
        return StreamSupport.stream(utilisateurs.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @PostMapping("")
    Utilisateur create(@RequestBody Utilisateur nouvelUtilisateur) {
        return utilisateurs.save(nouvelUtilisateur);
    }

    @PostMapping("/{id}/role/{roleId}")
    Utilisateur addRole(@PathVariable Long id, @PathVariable Long roleId) {
        Utilisateur utilisateurAuquelAjouterUnRole = this.byId(id);
        utilisateurAuquelAjouterUnRole.ajouterUnRole(roles.findById(roleId).orElseThrow());

        return utilisateurs.save(utilisateurAuquelAjouterUnRole);
    }

    // Single item

    @GetMapping("/{id}")
    Utilisateur byId(@PathVariable long id) {
        return utilisateurs.findById(id).orElseThrow();
    }

    @PutMapping("/{id}")
    Utilisateur update(@RequestBody Utilisateur modifications, @PathVariable Long id) {
        return utilisateurs.findById(id)
                .map(utilisateurAModifier -> {
                    utilisateurAModifier.setLogin(modifications.getLogin());
                    utilisateurAModifier.setMotDePasse(modifications.getMotDePasse());
                    utilisateurAModifier.setRoles(modifications.getRoles());
                    return utilisateurs.save(utilisateurAModifier);
                })
                .orElseGet(() -> utilisateurs.save(modifications));
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        utilisateurs.deleteById(id);
    }
}
