package kilya.iut.r5a05.article;

import kilya.iut.r5a05.utilisateur.UtilisateurRepository;
import kilya.iut.r5a05.utilisateur.Utilisateur;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping(value = "/article")
public class ArticleController {
    private final ArticleRepository articles;
    private final UtilisateurRepository utilisateurs;

    ArticleController(
            ArticleRepository articles,
            UtilisateurRepository utilisateurs
    ) {
        this.articles = articles;
        this.utilisateurs = utilisateurs;
    }

    @GetMapping("")
    List<Article> all() {
        return StreamSupport.stream(articles.findAll().spliterator(), false).collect(Collectors.toList());
    }
    // end::get-aggregate-root[]

    @PostMapping("")
    Article create(@RequestBody Article nouvelArticle) {
        return articles.save(nouvelArticle);
    }

    // Single item

    @GetMapping("/{id}")
    Article byId(@PathVariable Long id) {

        return articles.findById(id).orElseThrow();
    }

    @PutMapping("/{id}")
    Article update(@RequestBody Article modifications, @PathVariable Long id) {
        return articles.findById(id)
                .map(articleAModifier -> {
                    articleAModifier.setTitre(modifications.getTitre());
                    articleAModifier.setContenu(modifications.getContenu());
                    articleAModifier.setAuteur(modifications.getAuteur());
                    articleAModifier.setDateDePublication(modifications.getDateDePublication());
                    return articles.save(articleAModifier);
                })
                .orElseGet(() -> articles.save(modifications));
    }

    @PostMapping("/{article_id}/like")
    Article like(@RequestBody DonnerSonAvisCommand command, @PathVariable Long article_id) {
        Article aLiker = this.articles.findById(article_id).orElseThrow();
        Utilisateur quiDonneSonAvis = utilisateurs.findById(command.getUtilisateurQuiDonneSonAvisId()).orElseThrow();
        aLiker.liker(quiDonneSonAvis);
        return articles.save(aLiker);
    }

    @PostMapping("/{article_id}/dislike")
    Article dislike(@RequestBody DonnerSonAvisCommand command, @PathVariable Long article_id) {
        Article aDisliker = this.articles.findById(article_id).orElseThrow();
        Utilisateur quiDonneSonAvis = utilisateurs.findById(command.getUtilisateurQuiDonneSonAvisId()).orElseThrow();
        aDisliker.disliker(quiDonneSonAvis);
        return articles.save(aDisliker);
    }

    @DeleteMapping("/{id}")
    void delete(@PathVariable Long id) {
        articles.deleteById(id);
    }
}
