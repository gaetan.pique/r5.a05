package kilya.iut.r5a05.article;

import org.springframework.data.repository.CrudRepository;

interface AvisRepository extends CrudRepository<Avis, Long> {
}
