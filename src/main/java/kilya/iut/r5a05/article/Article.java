package kilya.iut.r5a05.article;

import kilya.iut.r5a05.utilisateur.Utilisateur;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Entity
@Table
@Getter
@Setter
public class Article {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long articleId;

    private String titre;
    private String contenu;
    private LocalDate dateDePublication;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "article_id")
    private List<Avis> avis;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "auteur_id")
    private Utilisateur auteur;

    public void liker(Utilisateur utilisateurQuiDonneSonAvis) {
        this.ajouterUnAvis(utilisateurQuiDonneSonAvis, true);
    }

    public void disliker(Utilisateur utilisateurQuiDonneSonAvis) {
        this.ajouterUnAvis(utilisateurQuiDonneSonAvis, false);
    }

    private void ajouterUnAvis(Utilisateur utilisateurQuiDonneSonAvis, boolean like) {
        Optional<Avis> avisExistant = this.avis.stream().filter(a -> a.getUtilisateur().getLogin().equals(utilisateurQuiDonneSonAvis.getLogin())).findAny();

        if (avisExistant.isPresent()) {
            avisExistant.get().setLikeOuDislike(like);
        } else {
            this.avis.add(new Avis(utilisateurQuiDonneSonAvis, like));
        }
    }
}
