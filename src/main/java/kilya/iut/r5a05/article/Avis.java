package kilya.iut.r5a05.article;

import kilya.iut.r5a05.utilisateur.Utilisateur;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
public class Avis {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long avisId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "utilisateur")
    private Utilisateur utilisateur;

    private boolean likeOuDislike;

    public Avis(
            Utilisateur utilisateur,
            boolean likeOuDislike
    ) {
        this.utilisateur = utilisateur;
        this.likeOuDislike = likeOuDislike;
    }
}
