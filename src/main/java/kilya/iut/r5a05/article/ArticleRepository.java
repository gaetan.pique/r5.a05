package kilya.iut.r5a05.article;

import org.springframework.data.repository.CrudRepository;

interface ArticleRepository extends CrudRepository<Article, Long> {
}
